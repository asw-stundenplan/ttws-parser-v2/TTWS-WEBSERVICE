The /timetable-endpoint allows to query the asw timetable. It can create a JSON or XML-Response based on the "Accept"-header transmitted by the client.
Both formats will be send in the same logical structure, which means that they can be handeled by one class-model.
The class-model will be explained below and is split in 2 parts:
  - Part 1: Generic container. This container will provide informations about errors which accour while processing the request. This container is used gloablly for ALL endpoints. The "data"-Variable contains the timetable-object
  - Part 2: Timetable-structure. This structure contains the data of the timetable. Please note, that this structure will only be delived if there were no processing errors

NOTE: Instead of reading this document you can als take a look at the java-reference-implementation located here: https://gitlab.com/asw-stundenplan/TTWS-JAVA-API
NOTE: The XSD for all XML-Endpoints can be found here: /generic/help/resources/xsd

Part 1: Container:
Class: RequestResult
       - boolean:   succesful
	   - string:    errorMessage
	   - Timetable: data
	   - ErrorCode: errorCode
	   - string:    readableErrorMessage

Enum: ErrorCode: (Value: Description)
      - EC001: Missing parameter
      - EC400: ASW-Server returned HTTP 4XX
      - ES001: Could not fetch data from ASW-website
      - ES500: ASW-Server returned HTTP 5XX
      - EM001: Unknown error

JSON-Example:
{
  "successful":true,
  "errorMessage":"",
  "errorCode":"",
  "readableErrorMessage":"",
  "data":...
}

XML-Example:
<requestResult>
  <successful>true</successful>
  <errorMessage></errorMessage>
  <errorCode></errorCode>
  <readableErrorMessage></readableErrorMessage>
  <timetable>
    ...
  </timetable>
</requestResult>



Part 2: Timetable-structure:
Class: Timetable:
       - DateTime:        startDate
	   - DateTime:        endDate
	   - TimetableWeek[]: weeks

JSON-Example:
{
  "weeks":[...],
  "startDate":"2018-02-25T23:00:00.000+0000",
  "endDate":"2018-03-23T23:00:00.000+0000"
}

XML-Example:
<weeks>
  <week>...</week>
  <startDate>2018-02-26T00:00:00+01:00</startDate>
  <endDate>2018-03-24T00:00:00+01:00</endDate>
</weeks>




Class: TimetableWeek:
       - DateTime:        startDate
	   - DateTime:        endDate
	   - TimetableDays[]: days

JSON-Example:
{
  "days":[...],
  "startDate":"2018-02-25T23:00:00.000+0000",
  "endDate":"2018-03-01T23:00:00.000+0000"
}

XML-Example:
<week>
  <days>...</days>
  <startDate>2018-02-26T00:00:00+01:00</startDate>
  <endDate>2018-03-02T00:00:00+01:00</endDate>
</week>




Class: TimetableDay
       - DateTime:        date
	   - TimetableHour[]: hours

JSON-Example:
{
  "date":"2018-02-25T23:00:00.000+0000",
  "hours":[...]
}

XML-Example:
<day>
  <date>2018-02-26T00:00:00+01:00</date>
  <hours>...</hours>
</day>




Class: TimetableHour:
       - DateTime: startTime
	   - DateTime: endTime
	   - String:   subject
	   - String:   room
	   - String:   location
	   - String:   type

JSON-Example:
{
  "startTime":"1970-01-01T12:00:00.000+0000",
  "endTime":"1970-01-01T13:30:00.000+0000",
  "subject":"CONTROL",
  "room":"2.13",
  "location":"NK",
  "type":"Vorlesung"
}

XML-Example:
<hour>
  <startTime>1970-01-01T13:00:00+01:00</startTime>
  <endTime>1970-01-01T14:30:00+01:00</endTime>
  <subject>CONTROL</subject>
  <room>2.13</room>
  <location>NK</location>
  <type>Vorlesung</type>
</hour>