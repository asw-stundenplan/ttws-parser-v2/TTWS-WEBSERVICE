package de.notepass.ttws.webservice;

import de.notepass.ttws.core.timetable.TimeTableParser;
import de.notepass.ttws.core.cachelayer.CachedTimetableParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        TimeTableParser.bootstrap();
        CachedTimetableParser.init();
    }
}