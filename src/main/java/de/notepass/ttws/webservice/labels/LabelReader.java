package de.notepass.ttws.webservice.labels;

import de.notepass.ttws.api.java.ErrorCode;

import java.util.ResourceBundle;

public class LabelReader {
    public static ResourceBundle errorMessages = ResourceBundle.getBundle("labels.errorMessages");

    public static String getErrorMessage(ErrorCode errorCode, String ... args) {
        return String.format(errorMessages.getString("label.errorMessage."+errorCode.name()), (Object[]) args);
    }
}
