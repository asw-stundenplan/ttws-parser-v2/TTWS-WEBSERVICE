package de.notepass.ttws.webservice.api;

import de.notepass.ttws.api.java.pojo.classes.AswClassCollection;
import de.notepass.ttws.core.classes.ClassesCalculator;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClassesController {
    @RequestMapping("/classes/getAllActiveClasses")
    public String getAllActiveClasses() {
        return "WIP"; //TODO: Returns all classes which are currently in a theory block
    }

    @RequestMapping(value = "/classes/getAllClasses", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public AswClassCollection getAllClasses() {
        return ClassesCalculator.getExistingClasses(); //TODO: Caching
    }
}
