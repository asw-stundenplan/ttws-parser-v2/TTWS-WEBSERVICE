package de.notepass.ttws.webservice.api.help;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This class generates help-texts for the response model
 */
@RestController
public class HelpController {
    public static String timetableHelpText = null;
    public static String xsd = null;

    private static void loadHelptext() {
        if (timetableHelpText == null) {
            try {
                timetableHelpText = loadTextContent(HelpController.class.getResource("/help/text/helptext.timetable.txt"));
            } catch (Exception e) {
                timetableHelpText = "Could not load helptext for timetable: " + e.getClass() + ":" + e.getMessage();
                //TODO Logging
            }
        }
    }

    private static void loadXsdContent() {
        if (xsd == null) {
            try {
                xsd = loadTextContent(HelpController.class.getResource("/help/resources/RequestResult.xsd"));
            } catch (Exception e) {
                timetableHelpText = "Could not XSD-file: " + e.getClass() + ":" + e.getMessage();
                //TODO Logging
            }
        }
    }

    private static String loadTextContent(URL location) throws URISyntaxException, IOException {
        return new String(
                Files.readAllBytes(
                        Paths.get(
                                location.toURI()
                        )
                ),
                StandardCharsets.UTF_8
        );
    }

    @RequestMapping(value = "/timetable/help", produces = "text/plain")
    public String generateTimetableHelp() {
        loadHelptext();
        return timetableHelpText;
    }

    @RequestMapping(value = "/generic/help/resources/xsd", produces = "text/xml")
    public String getTimetableXsd() {
        loadXsdContent();
        return xsd;
    }
}
