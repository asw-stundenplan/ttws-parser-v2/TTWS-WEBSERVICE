package de.notepass.ttws.webservice.api;

import de.notepass.ttws.api.java.ErrorCode;
import de.notepass.ttws.api.java.pojo.classes.AswClass;
import de.notepass.ttws.api.java.pojo.classes.AswClassCollection;
import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.api.java.pojo.RequestResult;
import de.notepass.ttws.core.cachelayer.CachedTimetableParser;
import de.notepass.ttws.core.classes.ClassesCalculator;
import de.notepass.ttws.core.error.TtwsException;
import de.notepass.ttws.webservice.labels.LabelReader;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TimetableController {

    @RequestMapping(value = "/timetable/getBlock", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public RequestResult<Timetable> getTimetableBlock(
            @RequestParam(value = "class") String clazz,
            @RequestParam(value = "block") int block
    ) {
        RequestResult<Timetable> result = new RequestResult<Timetable>();
        try {
            Timetable t = CachedTimetableParser.getTimetableForBlock(clazz, block);
            result.setSuccessful(true);
            result.setErrorMessage("");
            result.setData(t);
        } catch (TtwsException e) {
            handleInternalError(result, e);
        } catch (Exception e) {
            handleNonInternalError(result, e);
        }
        return result;
    }

    @RequestMapping(value = "/timetable/getBlockRange", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public RequestResult<Timetable> getTimetableBlockRange(
            @RequestParam(value = "class") String clazz,
            @RequestParam(value = "startBlock") int startBlock,
            @RequestParam(value = "endBlock") int endBlock
    ) {
        RequestResult<Timetable> result = new RequestResult<Timetable>();
        try {
            Timetable t = CachedTimetableParser.getTimetableForBlockRange(clazz, startBlock, endBlock);
            result.setSuccessful(true);
            result.setErrorMessage("");
            result.setData(t);
        } catch (TtwsException e) {
            handleInternalError(result, e);
        } catch (Exception e) {
            handleNonInternalError(result, e);
        }
        return result;
    }

    //@RequestMapping(value = "/timetable/getAllTimetables", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public RequestResult<List<Timetable>> getAllTimetables() {
        RequestResult<List<Timetable>> result = new RequestResult<>();
        try {
            List<Timetable> timetables = new ArrayList<>();
            AswClassCollection classCollection = ClassesCalculator.getExistingClasses();
            for (AswClass clazz:classCollection.getClasses()) {
                try {
                    timetables.add(CachedTimetableParser.getTimetableForBlockRange(clazz.getShortId(), clazz.getCurrentBlocks()[0], clazz.getCurrentBlocks()[clazz.getCurrentBlocks().length - 1]));
                } catch (Exception e) {
                    //TODO: Logging
                    System.err.println("Failed to load "+clazz.getShortId());
                }
            }

            result.setSuccessful(true);
            result.setErrorMessage("");
            result.setData(timetables);
        } catch (Exception e) {
            handleNonInternalError(result, e);
        }
        return result;
    }

    @RequestMapping(value = "/timetable", produces = "text/html")
    public String doRootRedirectionText() {
        return "There is nothing here. Please visit <a href='/timetable/help'>the help page</a> for developer-information.";
    }

    /**
     * This method handels all exceptions that are not TtwsException-instances
     * @param result
     * @param e
     */
    private void handleNonInternalError(RequestResult result, Exception e) {
        result.setSuccessful(false);
        result.setErrorMessage(e.getClass().getName() + ": " + e.getMessage());
        Throwable t = e.getCause();
        result.setErrorCode(ErrorCode.EM001);
        result.setReadableErrorMessage(LabelReader.getErrorMessage(ErrorCode.EM001, e.getClass().getName() + ": " + e.getMessage()));
        e.printStackTrace(); //TODO: Logging
    }

    /**
     * This method handels all exceptions that are TtwsException-instances
     * @param result
     * @param e
     */
    private void handleInternalError(RequestResult result, TtwsException e) {
        result.setSuccessful(false);
        result.setErrorMessage(e.getClass().getName() + ": " + e.getMessage());
        result.setErrorCode(e.getErrorCode());
        result.setReadableErrorMessage(LabelReader.getErrorMessage(e.getErrorCode(), e.getErrorMessagePlaceholders()));
        e.printStackTrace(); //TODO: Logging
    }
}