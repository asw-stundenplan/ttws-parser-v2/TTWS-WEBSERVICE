package de.notepass.ttws.webservice.ical;

import de.notepass.ttws.api.java.pojo.RequestResult;
import de.notepass.ttws.api.java.pojo.classes.AswClass;
import de.notepass.ttws.api.java.pojo.classes.AswClassCollection;
import de.notepass.ttws.api.java.pojo.timetable.Timetable;
import de.notepass.ttws.api.java.pojo.timetable.TimetableDay;
import de.notepass.ttws.api.java.pojo.timetable.TimetableHour;
import de.notepass.ttws.api.java.pojo.timetable.TimetableWeek;
import de.notepass.ttws.core.cachelayer.CachedTimetableParser;
import de.notepass.ttws.core.classes.ClassesCalculator;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Version;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by kim on 24.02.2018.
 */
@RestController
public class IcalController {
    @RequestMapping(value = "/ICAL/checkCalendar", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public RequestResult checkCalendar(@RequestParam(value = "class") String clazz) {
        RequestResult result = new RequestResult();
        try {
            generateCal(clazz);
            return result;
        } catch (Exception e) {
            result.setSuccessful(false);
            return result;
        }
    }

    @RequestMapping("/ICAL/getCalendar")
    public String getCalendar(@RequestParam(value = "class") String clazz) {
        try {
            return generateCal(clazz);
        } catch (Exception e) {
            //TODO: Logging
            e.printStackTrace();
            return "";
        }
    }


    public String generateCal(String clazz) throws Exception {
        //1. Get current classes
        AswClassCollection classCollection = ClassesCalculator.getExistingClasses();
        int startBlock = 0;
        int endBlock = 0;

        //2. Get the currently active blocks of the requested class
        for (AswClass ctx : classCollection.getClasses()) {
            if (ctx.getShortId().equalsIgnoreCase(clazz)) {
                startBlock = ctx.getCurrentBlocks()[0];
                endBlock = ctx.getCurrentBlocks()[ctx.getCurrentBlocks().length - 1];
                break;
            }
        }

        //3. Get the timetable for the range
        Timetable tt = CachedTimetableParser.getTimetableForBlockRange(clazz, startBlock, endBlock);

        Calendar calendar = new Calendar();
        calendar.getProperties().add(new ProdId("-//TimetableWebService 2.0//DE"));
        calendar.getProperties().add(Version.VERSION_2_0);
        calendar.getProperties().add(CalScale.GREGORIAN);
        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        TimeZone timezone = registry.getTimeZone("Europe/Berlin");
        VTimeZone tz = timezone.getVTimeZone();
        calendar.getComponents().add(tz);

        //4. Generate calendar
        for (TimetableWeek week : tt.getWeeks()) {
            for (TimetableDay day : week.getDays()) {
                for (TimetableHour hour : day.getHours()) {
                    SimpleDateFormat fuckIt = new SimpleDateFormat("dd.MM.yyyy|HH:mm");
                    String tmp = fuckIt.format(day.getDate());
                    String tmp2 = fuckIt.format(hour.getStartTime());
                    String tmp3 = fuckIt.format(hour.getEndTime());
                    java.util.Date startDate = fuckIt.parse(tmp.substring(0, tmp.indexOf('|')) + tmp2.substring(tmp2.indexOf('|')));
                    java.util.Date endDate = fuckIt.parse(tmp.substring(0, tmp.indexOf('|')) + tmp3.substring(tmp2.indexOf('|')));

                    VEvent tthourIcal = new VEvent(
                            new DateTime(startDate),
                            new DateTime(endDate),
                            (hour.getType().toLowerCase().equals("klausur") ? "Klausur: " : "") +
                                    (hour.getType().toLowerCase().equals("SA") ? "Studienarbeit" : "") +
                                    hour.getSubject() + (hour.getRoom().trim().equals("") ? "" : " (" + hour.getRoom() + ")")
                    );
                    tthourIcal.getProperties().add(new Location(hour.getLocation() + ": " + hour.getRoom()));
                    calendar.getComponents().add(tthourIcal);
                }
            }
        }

        return calendar.toString();
    }
}
